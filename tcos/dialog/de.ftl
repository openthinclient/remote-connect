accept-connection--title = Remote Verbindungsversuch
accept-connection-message = Wollen Sie die Verbindung auf Ihren Bildschirm zulassen?
accept-connection-button-confirm = Zulassen
accept-connection-button-cancel = Verweigern

disconnect-window-title = VNC Verbindung
disconnect-title = Remoteverbindung trennen?
disconnect-message = Bitte schließen Sie diesen Dialog erst nach Aufforderung.
disconnect-button-confirm = Trennen
