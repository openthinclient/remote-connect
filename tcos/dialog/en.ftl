accept-connection-title = Remote view request
accept-connection-message = Do you want to allow the connection to your screen?
accept-connection-button-confirm = Accept
accept-connection-button-cancel = Decline

disconnect-window-title = VNC connection
disconnect-title = Disconnect remote view connect?
disconnect-message = Please close this dialog only after explicit request.
disconnect-button-confirm = Disconnect
